# RSA message Encoder v1.0
A little program that permit to send & receive messages encrypted with the classic assymetric algorithm : RSA
You can generate a new pair of keys, load different public keys or private keys and copy paste the generated result.

To work well with copy pastes, after encrypting with RSA algoritm, the resulted message is encoded in base64.

## requirements
You only need python3 and cryptography module installed

## Demos images : 
![Image of the GUI](Capture.PNG)