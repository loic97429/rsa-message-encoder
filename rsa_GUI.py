from tkinter import Frame, Tk, messagebox, Menu, filedialog
from tkinter import Button, Text, Label, Radiobutton
from tkinter import IntVar, StringVar
from tkinter import END, BOTTOM, LEFT, SUNKEN, W, X, GROOVE, DISABLED, NORMAL, TOP
import rsa_encryption as rsa
import os
import base64
os.environ["PYTHONIOENCODING"] = "utf-8"


cle_corres_file = None
cle_priv_file = None


def StatusBarUpdate():
    global cle_corres_file, cle_priv_file
    if int(choix.get()) == 1:
        if cle_corres_file:
            statusvar.set("Ready to encode with public key : " + cle_corres_file)
        else:
            statusvar.set("No public key loaded to encrypt message !")
    elif int(choix.get()) == 2:
        if cle_priv_file:
            statusvar.set("Ready to decode with private key : " + cle_priv_file)
        else:
            statusvar.set("No private key loaded to decode message !")
    else:
        statusvar.set("Preparing to do nothing...")


def StatusBarUpdatehelp(event=None):
    if fenetre1.call(event.widget, "index", "active") == 0:
        statusvar.set("About This Application")
    elif fenetre1.call(event.widget, "index", "active") == 1:
        statusvar.set("How to use this application")
    else:
        StatusBarUpdate()


def paste_menu(event=None):
    text1 = fenetre1.selection_get(selection="CLIPBOARD")
    entry.delete("1.0", END)
    entry.insert(END, text1)


def copy_menu(event=None):
    fenetre1.clipboard_clear()
    text1 = label3.get("1.0", END)
    fenetre1.clipboard_append(text1)


def onAbout():
    messagebox.showinfo("A propos :", "Par Dubard Loic, libre de droit")


def onHelp(event=None):
    messagebox.showinfo("Aide:", "comment utiliser le logiciel ?\n -Si vous voulez copier le contenu du resultat, vous devez faire Ctrl+c directement sans avoir a selectionner le texte.")


def quitter_fenetre1(event=None):
    global fenetre1
    var = messagebox.askokcancel("Quitter ?", "Voulez vous vraiment quitter ?")
    # print var
    if var:
        fenetre1.destroy()


def cle_corres():
    global cle_corres_file
    cle_corres_file = filedialog.askopenfilename()


def cle_priv():
    global cle_priv_file
    cle_priv_file = filedialog.askopenfilename()


def process_click():
    label3.config(state=NORMAL)
    label3.delete("1.0", END)
    message = entry.get("1.0", END).encode("utf-8")
    if int(choix.get()) == 1:
        if cle_corres_file and len(message) <= 2048:
            to_write = base64.b64encode(rsa.encryptMessage(message, cle_corres_file))
        elif len(message) > 2048:
            messagebox.showerror("Message trop long !", "Les algorithmes de cryptographie assymétrique ne permettent pas de chiffrer des données de taille plus grande que la clé (ici 2048 bytes) !")
        else:
            messagebox.showerror("ERREUR !", "Veuillez selectionner la clé publique du correspondant avant de chiffrer le message !")
    else:
        if cle_corres_file:
            try:
                to_write = rsa.decryptMessage(base64.b64decode(message), cle_priv_file).decode("utf-8")
            except Exception:
                messagebox.showerror("ERREUR !", "Peut-être n'est-ce pas la bonne clé ?")
        else:
            messagebox.showerror("ERREUR !", "Veuillez selectionner votre clé privée avant de déchiffrer le message !")
    label3.insert(END, to_write)
    label3.config(state=DISABLED)


def generate_keys():
    global cle_priv_file
    var = messagebox.askokcancel("Nouvelles clés ?", "Etes-vous sur de vouloir regénerer un nouvelle paire de clé ?")
    if var:
        files = rsa.generateKeys()
        messagebox.showinfo("Succès:", f"Les nouvelles clés {files} sont dans le répertoire : {os.getcwd()}")
        cle_priv_file = files[0]
        messagebox.showwarning("Attention:", f"Ne partagez surtout pas la clé privée : {files[0]} !")


def process_a_file():
    filename = filedialog.askopenfilename()
    if int(choix.get()) == 1:
        if cle_corres_file:
            rsa.encryptFile(filename, cle_corres_file)
            messagebox.showinfo("Succès !", f"Le fichier à été encrypté en : {filename}.dat")
        else:
            messagebox.showerror("ERREUR !", "Veuillez selectionner la clé publique du correspondant avant de chiffrer le message !")
    else:
        if cle_corres_file:
            try:
                rsa.decryptFile(filename, cle_priv_file)
                messagebox.showinfo("Succès !", f"Le fichier à été décrypté en : {os.path.splitext(filename)[0]}")
            except Exception:
                messagebox.showerror("ERREUR !", "Peut-être n'est-ce pas la bonne clé ?")
        else:
            messagebox.showerror("ERREUR !", "Veuillez selectionner votre clé privée avant de déchiffrer le message !")


if __name__ == "__main__":
    fenetre1 = Tk()
    fenetre1.title("RSA message Encoder v1.0")
    fenetre1["bg"] = "green"

    # menu
    menu = Menu(fenetre1)
    fenetre1.config(menu=menu)

    filemenu = Menu(menu, tearoff=0)
    menu.add_cascade(label="File", menu=filemenu)
    filemenu.add_command(label="Exit", command=quitter_fenetre1, accelerator="Ctrl+q")
    fenetre1.bind_all("<Control-q>", quitter_fenetre1)

    edit = Menu(menu, tearoff=0)
    menu.add_cascade(label="Edit", menu=edit)
    edit.add_command(label="copy (result)", command=copy_menu, accelerator="Ctrl+c")
    edit.add_command(label="paste (in entrybox)", command=paste_menu, accelerator="Ctrl+v")
    fenetre1.bind_all("<Control-c>", copy_menu)
    fenetre1.bind_all("<Control-v>", paste_menu)

    helpmenu = Menu(menu, tearoff=0)
    menu.add_cascade(label="Help", menu=helpmenu)
    helpmenu.add_command(label="About", command=onAbout)
    helpmenu.add_command(label="Help", command=onHelp, accelerator="F11")
    helpmenu.bind("<<MenuSelect>>", StatusBarUpdatehelp)
    fenetre1.bind_all("<F11>", onHelp)

    # status bar
    statusvar = StringVar()
    status = Label(fenetre1, text="Preparing to do nothing...", bd=1, relief=SUNKEN, anchor=W, textvariable=statusvar)
    status.pack(side=BOTTOM, fill=X)
    statusvar.set("Preparing to do nothing...")

    # frames
    cadre1 = Frame(fenetre1, borderwidth=2, relief=GROOVE)
    cadre1.pack(padx=10, pady=10)

    cadre2 = Frame(cadre1, relief=GROOVE)
    cadre2.pack(side=LEFT, padx=10, pady=10)

    cadre3 = Frame(cadre1, relief=GROOVE)
    cadre3.pack(side=TOP, padx=20, pady=30)

    cadre4 = Frame(cadre1, relief=GROOVE)
    cadre4.pack(side=BOTTOM, padx=20, pady=30)

    # widgets cadre2
    label1 = Label(cadre2, text="entrez texte :").pack()

    entry = Text(cadre2, height=10, width=30)
    entry.pack()

    label2 = Label(cadre2, text="resultat :").pack(pady=20)

    label3 = Text(cadre2, height=10, bg="white", width=30, state=DISABLED)
    label3.pack(pady=5)

    # widgets cadre3
    Button(cadre3, text="clé publique du correspondant", command=cle_corres).grid(row=0, column=0)
    Button(cadre3, text="ma clé privée", command=cle_priv).grid(row=0, column=1)
    Button(cadre3, text="Génerer un nouvelle paire de clé", command=generate_keys).grid(row=1, column=0, columnspan=2, pady=20)

    # widgets cadre 4
    choix = IntVar()
    choix1 = Radiobutton(cadre4, text="Crypter", variable=choix, value=1, command=StatusBarUpdate)
    choix1.pack()
    choix2 = Radiobutton(cadre4, text="Decrypter", variable=choix, value=2, command=StatusBarUpdate)
    choix2.pack()

    Button(cadre4, text="Proceder sur le message", command=process_click).pack(pady=30)
    Button(cadre4, text="Proceder sur un ficher", command=process_a_file).pack(pady=30)

    fenetre1.resizable(False, False)

    fenetre1.mainloop()
