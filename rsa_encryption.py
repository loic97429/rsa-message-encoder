from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import padding, rsa
from getpass import getuser
from cryptography.fernet import Fernet
import os


algorithm = hashes.SHA256()
mgf = padding.MGF1(algorithm=algorithm)
oaep = padding.OAEP(mgf=mgf, algorithm=algorithm, label=None)


def generateKeys(filename=getuser() + "_key", size=2048):
    private_key = rsa.generate_private_key(public_exponent=65537, key_size=size, backend=default_backend())
    public_key = private_key.public_key()
    pem = private_key.private_bytes(encoding=serialization.Encoding.PEM, format=serialization.PrivateFormat.TraditionalOpenSSL, encryption_algorithm=serialization.NoEncryption())
    public_pem = public_key.public_bytes(encoding=serialization.Encoding.OpenSSH, format=serialization.PublicFormat.OpenSSH)
    with open(filename + '.pem', 'wb') as f:
        f.write(pem)
    with open(filename + ".pub", 'wb') as f:
        f.write(public_pem)
    return filename + '.pem', filename + ".pub"


def encryptMessage(message, public_key_file):
    try:
        with open(public_key_file, "rb") as f:
            public_key = serialization.load_pem_public_key(f.read(), backend=default_backend())
    except ValueError:
        with open(public_key_file, "rb") as f:
            public_key = serialization.load_ssh_public_key(f.read(), backend=default_backend())

    return public_key.encrypt(message, oaep)


def decryptMessage(message, private_key_file, password=None):
    with open(private_key_file, 'rb') as f:
        private_key = serialization.load_pem_private_key(f.read(), password=password, backend=default_backend())
    return private_key.decrypt(message, oaep)


def decryptFile(filename, private_key_file, password=None):
    with open(filename, "rb") as f:
        content = f.read()
    key = decryptMessage(content[:256], private_key_file, password)
    fernet = Fernet(key)
    decrypted_content = fernet.decrypt(content[256:])
    with open(os.path.splitext(filename)[0], "wb") as f:
        f.write(decrypted_content)


def encryptFile(filename, public_key_file):
    with open(filename, "rb") as f:
        content = f.read()
    key = Fernet.generate_key()
    fernet = Fernet(key)
    encrypted_key = encryptMessage(key, public_key_file)
    encrypted_content = fernet.encrypt(content)
    with open(filename + ".dat", "wb") as f:
        f.write(encrypted_key)
        f.write(encrypted_content)


if __name__ == "__main__":
    import base64
    # generateKeys()
    cipher = encryptMessage("Bonjour".encode('utf-8'), "wikle_key.pub")
    print(len(base64.b64encode(cipher)))
    print(decryptMessage(cipher, "wikle_key.pem").decode('utf-8'))
